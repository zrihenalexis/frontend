const initialState = {
  suppliers: {
    content:[]
  },
  supplier: {
    name: '',
    contactPersonList: []
  },
  qualitySurvey: {
    values :{
      content: []
    }
  },
  isLoading: false
};

export default initialState;
